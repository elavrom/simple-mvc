<?php // index.php Frontend Controller / Router
// On charge toutes les classes du projet.
// Si on avait composer, on aurait l'autoloader qui permet de les charger automatiquement
require_once './MyController.php';
require_once './MyObject.php';
require_once './MyObjectDAO.php';
$dao = new MyObjectDAO();
global $dao;
$backendController = new MyController();
switch ($_SERVER['PATH_INFO']) {
    case '/show': echo $backendController->showObject();
    exit;
    case '/save': echo $backendController->saveObject();
    exit;
    default: echo $backendController->createObject();
    exit;
}
