<?php // MyObjectDAO.php - Model
class MyObjectDAO
{
    private ?\PDO $connexion;

    public function __construct()
    {
        $this->connexion = new \PDO('sqlite:./db.sqlite');
    }
    public function save(MyObject $myObject)
    {
        $this->connexion->prepare("INSERT INTO MyTable (data)
 VALUES (:data)")
            ->execute(['data' => $myObject->getData()]);
        $id = $this->connexion->query("SELECT last_insert_rowid()")->fetchColumn();
        $myObject->setId($id);
    }
    public function getMyObject($id)
    {
        $myObject = (new MyObject())->setId($id);
        $stmt = $this->connexion->prepare("SELECT data FROM MyTable
WHERE id = :id");
        $stmt->execute(['id' => $id]);
        $myObject->setData($stmt->fetchColumn());
        return $myObject;
    }
}