<?php // MyObject.php - Model
class MyObject
{
    private $id;
    private $data;
    public function __construct($data = null) {
        $this->data = $data;
    }
    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getData() {
        return $this->data;
    }
    public function setData($data) {
        $this->data = $data;
        return $this;
    }
}