<!-- save_object.php - View -->
<h1>Bravo !</h1>
<p>Vous avez bien sauvegardé votre objet
(ID : <?= $object->getId() ?>).
    <a href="/show?id=<?=$object->getId() ?>">
        Aller voir mon objet
    </a>
    <a href="/">Créer un nouvel objet</a>
</p>