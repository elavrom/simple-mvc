<?php // MyController.php - Backend Controller
class MyController
{
    public function createObject() {
        return $this->render('create_object.php', []);
    }
    public function showObject() {
        $myObject = $this->getDAO()->getMyObject($_GET['id']);
        return $this->render('show_object.php',
            ['object' => $myObject]);
    }
    public function saveObject() {
        $myObject = new MyObject($_POST['data']);
        $this->getDAO()->save($myObject);
        return $this->render('save_object.php',
            ['object' => $myObject]);
    }
    private function getDAO() {
        global $dao;
        return $dao;
    }
    private function render($template, $args) {
        extract($args, EXTR_OVERWRITE);
        ob_start();
        require "./$template";
        return ob_get_clean();
    }
}